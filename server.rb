# frozen_string_literal: true

require 'sinatra'
require './game.rb'

enable :sessions

get('/') { Game.new(session).index }
get('/image') { Game.new(session).image }
get('/set/:value') { Game.new(session).update(params['value']) }
