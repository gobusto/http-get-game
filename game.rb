# frozen_string_literal: true

# This is where the actual logic lives - separate from the server routing.
class Game
  def initialize(session)
    @session = session
  end

  def index
    render(:index)
  end

  def image
    define_cookie! # If this is missing later, then cookies aren't enabled.

    name = @session['value'] || 'default'
    render("images/#{name}", ext: 'jpg')
  end

  def update(value)
    return render(:cookies) unless cookies_enabled?
    return render(:error) unless value_allowed?(value)

    @session['value'] = value
    render(:update)
  end

  protected

  def define_cookie!
    @session['cookies_enabled'] = 'yes'
  end

  def cookies_enabled?
    @session['cookies_enabled'] == 'yes'
  end

  def render(name, ext: 'html')
    File.read("./files/#{name}.#{ext}")
  end

  private

  def value_allowed?(value)
    %w[asuka misato rei].include?(value)
  end
end
