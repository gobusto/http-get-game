The HTTP GET Game
=================

This is a basic proof-of-concept for a "game" played via HTML `<img>` and `<a>`
tags. The idea is that this can be embedded on pretty much any site where those
two tags are allowed - such as forums - for some basic interactivity.

Overview
--------

This concept is based around two URLs:

+ One responds with an image: `/image`
+ One stores a value: `/set/yourvaluehere`

There's also a third "index" URL (`/`), but this isn't technically required.

The idea is to have an `<img src="http://something.com/image" alt="image">` tag
respond with an image file _which people can change_ by clicking on one or more
`<a href="http://something.com/set/hello">Hello</a>` tags. These `<a>` tags set
a value on the server, which is then used to decide which image to send back in
the future.

Something like [Bamboozle!](https://en.wikipedia.org/wiki/Bamboozle_%28quiz%29)
would be fairly simple to set up: A title screen, 10 questions, and 11 possible
scores could be done with 22 image files plus an `<a>` tag for each of the Red,
Green, Yellow, and Blue "answer" buttons.

You could even have the server _generate_ images, rather than relying on a list
of static files...

Technical Details
-----------------

The actual web server bit is handled by [Sinatra](http://sinatrarb.com/):

    # frozen_string_literal: true

    require 'sinatra'
    require './game.rb'

    enable :sessions

    get('/') { Game.new(session).index }
    get('/image') { Game.new(session).image }
    get('/set/:value') { Game.new(session).update(params['value']) }

You can run this with `ruby server.rb`.

The actual game logic is implemented in `game.rb`, in a class called `Game`:

    # frozen_string_literal: true

    # This is where the actual logic lives - separate from the server routing.
    class Game
      def initialize(session)
        # ...
      end

      def index
        # ...
      end

      def image
        # ...
      end

      def update(value)
        # ...
      end
    end

The `index` and `update` methods are expected to return HTML; `image` returns a
stream of image data, probably via `File.read('./somefile.jpg')` (unless we are
generating the image dynamically, in which case It's A Bit More Complicated).

Note that this relies on cookies being enabled in order to store the session; I
found that the simplest way to verify this was as follows:

    # frozen_string_literal: true

    # This is where the actual logic lives - separate from the server routing.
    class Game
      def initialize(session)
        @session = session
      end

      def index
        # ...
      end

      def image
        define_cookie! # If this is missing later, then cookies aren't enabled.
        # ...
      end

      def update(value)
        return cookie_error unless cookies_enabled?
        # ...
      end

      private

      def define_cookie!
        @session['cookies_enabled'] = 'yes'
      end

      def cookies_enabled?
        @session['cookies_enabled'] == 'yes'
      end

      def cookie_error
        # ...
      end
    end

Of course, if you're not interested in trying to keep track of which visitor is
which, then you can simply ignore the session data and not worry about cookies.

Some notes about `<a>` tags
---------------------------

I'll write more here later... for now, just be aware that this is what I use to
send someone back to the original page after they've clicked an `<a>` tag:

    if (window.opener) {
      // Opened in a new tab; reload the original one and close this:
      window.opener.location.reload();
      window.close();
    } else if (document.referrer) {
      // This page has simply replaced the previous one; go back there:
      window.location.href = document.referrer;
    } else {
      // The user has come here directly - take them to the index page:
      window.location.href = '/';
    }

(The short version: Links with `rel='noopener'` force a new tab to open.)

Licensing
---------

Copyright (c) 2018 Thomas Glyn Dennis

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
of the Software, and to permit persons to whom the Software is furnished to do
so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

