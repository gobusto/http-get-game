Copyright Notice
================

The files in this directory are covered by the MIT/X11 license, as per the Ruby
scripts in the top-level directory. You may modify them however you wish.

However, please note that files in the `images/` subdirectory are not.
