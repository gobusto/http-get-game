Copyright Notice
================

Files in this directory belong to third parties and are protected by copyright.

They are included here under [Fair Use](https://en.wikipedia.org/wiki/Fair_use)
provisions, and - unlike the code itself - are _not_ licensed as MIT/X11.
